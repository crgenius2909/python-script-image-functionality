from flask import Flask
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
import os
from datetime import datetime
import csv
import pandas as pd
from werkzeug import secure_filename
import urllib
import PIL
from PIL import Image
app = Flask(__name__)
# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config['UPLOAD_RESIZEDFOLDER'] = 'uploads/Resizedimages'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
UPLOADING_FOLDER=os.path.join(BASE_DIR, 'uploads')
if not os.path.exists(os.path.join(BASE_DIR, 'uploads')+"/Resizedimages"):
    os.makedirs(os.path.join(BASE_DIR, 'uploads')+"/Resizedimages")
@app.route('/')
def index():
    return render_template('index.html')
def allowed_file(filename):
        return '.' in filename and \
        filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']
# Route that will process the file upload
@app.route('/upload', methods=['POST'])
def upload():
     # Get the name of the uploaded files
    uploaded_files = request.files.getlist("file[]")
    filenames = []
    for file in uploaded_files:
        #print(file)
        if file and allowed_file(file.filename):
            # Make the filename safe, remove unsupported chars
            filename = secure_filename(file.filename)
            # Move the file form the temporal folder to the upload
            # folder we setup
            imagename= os.path.splitext(file.filename)[0]
            now = datetime.now()
            extension=file.filename.rsplit('.', 1)[1]
            if(extension <> 'txt'):
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], imagename+"-"+"%s.%s" % (now.strftime("%Y-%m-%d-%H-%M-%S-%f"), file.filename.rsplit('.', 1)[1])))
                basewidth = 300
                #random_number = random.randint(1, 1000)
                #print(random_number)
                img = Image.open(UPLOADING_FOLDER+"/"+imagename+"-"+"%s.%s" % (now.strftime("%Y-%m-%d-%H-%M-%S-%f"), file.filename.rsplit('.', 1)[1]))
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img.save(os.path.join(app.config['UPLOAD_RESIZEDFOLDER'], imagename+"-"+"%s.%s" % (now.strftime("%Y-%m-%d-%H-%M-%S-%f"), file.filename.rsplit('.', 1)[1])))
                #img.save(os.path.join(app.config['UPLOAD_RESIZEDFOLDER'], filename))
                filenames.append(filename)
    if(extension=='txt'):
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        csvcreate = os.path.join('/uploads', 'final.csv')
        in_txt = csv.reader(open(UPLOADING_FOLDER+ "/" +filename, "rU"), delimiter = '\t')
        readfile=open(UPLOADING_FOLDER+ "/final.csv", 'wb')
        out_csv = csv.writer(readfile)
        out_csv.writerows(in_txt)
        readfile.close()
        frame = pd.read_csv(UPLOADING_FOLDER+ "/final.csv",sep='\s*,\s*',header=0, encoding='ascii', engine='python')
        df=frame['imagename'].astype(str)
        image_urls=df.tolist()
        for image_url in image_urls:
            file_name = image_url.split('/')[-1]
            imagename= os.path.splitext(file_name)[0]
            if(file_name <> 'nan'):
                testfile = urllib.URLopener()
                testfile.retrieve(image_url, UPLOADING_FOLDER+"/"+imagename+"-"+"%s.%s" % (now.strftime("%Y-%m-%d-%H-%M-%S-%f"), file_name.rsplit('.', 1)[1]))
                basewidth = 300
                img = Image.open(UPLOADING_FOLDER+"/"+imagename+"-"+"%s.%s" % (now.strftime("%Y-%m-%d-%H-%M-%S-%f"), file_name.rsplit('.', 1)[1]))
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img.save(os.path.join(app.config['UPLOAD_RESIZEDFOLDER'], imagename+"-"+"%s.%s" % (now.strftime("%Y-%m-%d-%H-%M-%S-%f"), file_name.rsplit('.', 1)[1])))
                filenames.append(file_name)
            #os.remove(UPLOADING_FOLDER+ "/final.csv")
    return render_template('upload.html', filenames=filenames)
@app.route('/uploads/<filename>')
def uploaded_file(filename):
        return send_from_directory(app.config['UPLOAD_FOLDER'],filename)
    
if __name__ == "__main__":
    app.run()